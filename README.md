# Robotic Vision Web User Interface (RV Web UI)

## Overview

The Robotic Vision Web User Interface (RV Web UI) is a light-weight webserver that provides a simple and easy to use interface for advertising, launching and interacting with demonstrators through any standard web browser.

On start-up, RV Web UI scrapes a set of pre-defined locations for directories in search of demonstrator modules that can be launched by RV Web UI. These demonstrator modules are indicated by the existance of a `module.yml` file and `html/index.html` with the root directory of the demonstrator.

*For more information on how to create a demonstrator module, please see our skeleton example located [here](https://bitbucket.org/acrv/example_demo).*

## Setup

### Requirements
- Ubuntu 18.04
- ROS Noetic

### Installing ROS
Follow the installation instructions provided for [ROS Noetic](http://wiki.ros.org/noetic/Installation/Ubuntu#Installation).

*Note: We recommend installing either the desktop or desktop-full distribution*

### Installing RV Web UI
The simplest approach to installing RV Web UI is through the use of apt. This ensures that you are on the latest release and that you will receive updates as they are released. Otherwise, feel free to clone this repository if you wish to play around.

#### APT Release

Import the Robotic Vision GPG Key using the following command
```
sudo -E apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv-key 5B76C9B0
```

Add the Robotic Vision repository to the apt sources list directory
```
sudo sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://roboticvision.org/packages $(lsb_release -sc) main" > /etc/apt/sources.list.d/acrv-latest.list'
```

Update your packages list
```
sudo apt update
```

Install RV Web UI by running the command
```
sudo apt install ros-noetic-rv-webui
```

This command will install RV Web UI and bring up a daemon service using systemd.

## Usage

### Accessing the Interface
To access RV Web UI simply navigate to [http://localhost/](http://localhost/) in your preferred web browser.

If the system has installed correctly, you should see something similar to the image below.

![picture](docs/dashboard.png)

Note that we use NGINX as a forward proxy for accessing the interface on port 80 - the standard HTTP port used by your browser (see /etc/nginx/sites-available/webui). If however another server is using this port, you can access the interface directly through  [http://localhost:8085/](http://localhost:8085/) 

### Launching a Demonstrator

To launch a loaded demonstrator simply click the **start** button for the demonstrator, then click **run** within the instructions pop-up. This will launch the demonstrator, bringing up any required software components, and navigating the browser to the demonstrator interface.

Importantly, only a single demonstrator can be active at any given time. Running a second demonstrator will cause any currently running demonstrator to shut-down before the newly selected demonstrator can come up. 

Further, a running demonstrator will remain active (regardless of whether the demonstrator interface is still open) until either a new demonstrator is selected, or the **reset** button on the RV Web UI main page is clicked.

### Configuring RV Web UI

During installation RV Web UI will create a default configuration file at  `/etc/rv_webui.conf` which can be edited to change the behaviour of the system. This file uses YAML syntax.

The following table describes the paramters that can currently be modified: 


| Parameter        | Default   | Required | Description  |
| ------------- |----|:----:|-----------|
| source      | /opt/ros/noetic/setup.bash | **No** | The indicated file will be used to generate the run-time environment when launching the demonstrators (see bash source command) |
| module_paths  |  /opt/ros/noetic/share | **No** | A list of paths to be crawled by RV Web UI while looking for `module.yml` files. This is a deep scan. Note that if `ROS_PACKAGE_PATH` is set it will be included by default in this set. |

After changing any values in this file you will need to restart the service by running the following command:

```
sudo service rv_webui restart
```

### Adding Modules

RV Web UI will automatically find and add any modules that are locatable based on the `module_paths` variable in `/etc/rv_webui.conf` as well as the set of paths in `ROS_PACKAGE_PATH` when it starts up.

If modules are added after startup, the set of available modules can be reloaded by accessing [http://localhost/reload](http://localhost/reload)

**Modules installed via apt will call this location during installation to automatically load themselves into the interface.**

